package ru.kuzin.tm.api.client;

import ru.kuzin.tm.dto.request.AbstractRequest;
import ru.kuzin.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}