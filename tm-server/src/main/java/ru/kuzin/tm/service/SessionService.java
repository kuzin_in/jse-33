package ru.kuzin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.ISessionRepository;
import ru.kuzin.tm.api.service.ISessionService;
import ru.kuzin.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}