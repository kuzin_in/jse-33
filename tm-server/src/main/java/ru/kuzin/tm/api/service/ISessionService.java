package ru.kuzin.tm.api.service;

import ru.kuzin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}