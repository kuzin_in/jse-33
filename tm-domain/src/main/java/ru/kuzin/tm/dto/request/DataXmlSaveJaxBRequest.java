package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveJaxBRequest extends AbstractUserRequest {

    public DataXmlSaveJaxBRequest(@Nullable final String token) {
        super(token);
    }

}