package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadJaxBRequest extends AbstractUserRequest {

    public DataJsonLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }

}