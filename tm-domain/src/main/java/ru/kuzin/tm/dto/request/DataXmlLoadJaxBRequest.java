package ru.kuzin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }

}